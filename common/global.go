package common

type IocGroup string

func (group IocGroup) Value() string {
	return string(group)
}

var (
	DefaultActiveGroup = "default"
	I18n               bool
)

var (
	RouterIocGroup           IocGroup = "router-group"
	ServerMiddlewareIocGroup IocGroup = "middleware"
	ServerTransIocGroup      IocGroup = "gotrans"
	ServerValidateIocGroup   IocGroup = "govalidator"
)
