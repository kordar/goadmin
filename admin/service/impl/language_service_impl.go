package impl

import (
	model "gitee.com/kordar/goadmin/admin/models"
	"gitee.com/kordar/goadmin/reader"
	"github.com/kordar/gocrud"
	gocrud_gorm "github.com/kordar/gocrud-gorm"
	"gorm.io/gorm"
)

type LanguageServiceImpl struct {
	db *gorm.DB
	gocrud.CommonResourceService
}

func NewLanguageService(db *gorm.DB) *LanguageServiceImpl {
	return &LanguageServiceImpl{db: db}
}

func (service LanguageServiceImpl) ResourceName() string {
	return "yaml"
}

func (service LanguageServiceImpl) Search(body gocrud.SearchBody) gocrud.SearchVO {
	searchBody := gocrud_gorm.NewGormSearchBody(body)
	db := searchBody.GormQuery(service.db, nil).Model(&model.SysLanguage{})
	queryReader := reader.NewQueryReader(db, body.Page, body.PageSize)
	//count := queryReader.Count()
	var data []*model.SysLanguage
	return queryReader.SearchVO(data, nil)
	//queryReader.Reader(&data)
	//list := queryReader.Datalist(data, len(data), func(index int, item interface{}) map[string]interface{} {
	//	m := item.(*model.SysLanguage)
	//	return map[string]interface{}{
	//		"_serial":     queryReader.AsSerial(index),
	//		"name":        m.Name,
	//		"create_time": m.CreateTime.Format("2006-01-02 15:04:05"),
	//		"update_time": m.UpdateTime.Format("2006-01-02 15:04:05"),
	//	}
	//})
	//return gocrud.NewSearchVO(list, count)
}

func (service LanguageServiceImpl) Delete(body gocrud.RemoveBody) error {
	return body.Delete(&model.SysLanguage{}, service.db, nil)
}

//func (service LanguageServiceImpl) Unique(ctx *gin.Context, data interface{}) error {
//	sysLanguage := data.(*model.SysLanguage)
//	if service.db.Where("name = ?", sysLanguage.Name).First(sysLanguage).Error == nil {
//		return errors.New("yaml.name.unique")
//	}
//	return nil
//}

func (service LanguageServiceImpl) Add(body gocrud.FormBody) (interface{}, error) {
	return body.CreateWithValid(&model.SysLanguage{}, service.db, nil, func(model interface{}) error {
		//return util.CtxGetValidParams(ctx, model, service, "Unique")
		return nil
	})
}

func (service LanguageServiceImpl) Update(body gocrud.FormBody) (interface{}, error) {
	return body.Update(&model.SysLanguage{}, service.db, nil)
}

func (service LanguageServiceImpl) Edit(body gocrud.EditorBody) error {
	return body.Updates(&model.SysLanguage{}, service.db, nil)
}
