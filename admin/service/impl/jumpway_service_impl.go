package impl

import (
	model "gitee.com/kordar/goadmin/admin/models"
	"gitee.com/kordar/goadmin/reader"
	"github.com/kordar/gocrud"
	gocrudgorm "github.com/kordar/gocrud-gorm"
	"gorm.io/gorm"
)

type JumpwayServiceImpl struct {
	db *gorm.DB
	*gocrud.CommonResourceService
}

func NewJumpwayService(db *gorm.DB) *JumpwayServiceImpl {
	return &JumpwayServiceImpl{db: db}
}

func (service JumpwayServiceImpl) ResourceName() string {
	return "jumpway"
}

func (service JumpwayServiceImpl) Search(body gocrud.SearchBody) gocrud.SearchVO {
	searchBody := gocrudgorm.NewGormSearchBody(body)
	db := searchBody.GormQuery(service.db, nil).Model(&model.SysJumpway{})
	queryReader := reader.NewQueryReader(db, body.Page, body.PageSize)
	var data []*model.SysJumpway
	queryReader.Reader(&data)
	return queryReader.SearchVO(data, nil)
	//list := queryReader.Datalist(data, len(data), func(index int, item interface{}) map[string]interface{} {
	//	m := item.(*model.SysJumpway)
	//	return map[string]interface{}{
	//		"_serial": queryReader.AsSerial(index),
	//		"id":      m.ID,
	//		"name":    m.Name,
	//		"type":    m.Type,
	//		"page":    m.Page,
	//		"params":  m.Params.V,
	//	}
	//})
	//return gocrud.NewSearchVO(list, queryReader.Count())
}

func (service JumpwayServiceImpl) Delete(body gocrud.RemoveBody) error {
	return body.Delete(&model.SysJumpway{}, service.db, nil)
}

//func (service JumpwayServiceImpl) Unique(ctx *gin.Context, data interface{}) error {
//	sysJumpway := data.(*model.SysJumpway)
//	if service.db.Where("icon = ?", sysJumpway.Name).First(sysJumpway).Error == nil {
//		locale := ctx.DefaultQuery("locale", "en")
//		value := gocfg.GetGroupSectionValue(locale, "system.jumpway.validate", "name.unique")
//		return errors.New(value)
//	}
//	return nil
//}

func (service JumpwayServiceImpl) Add(body gocrud.FormBody) (interface{}, error) {
	return body.CreateWithValid(&model.SysJumpway{}, service.db, nil, func(model interface{}) error {
		//return util.CtxGetValidParams(ctx, model, service, "Unique")
		return nil
	})
}

func (service JumpwayServiceImpl) Update(body gocrud.FormBody) (interface{}, error) {
	return body.Update(&model.SysJumpway{}, service.db, nil)
}

func (service JumpwayServiceImpl) Edit(body gocrud.EditorBody) error {
	return body.Updates(&model.SysJumpway{}, service.db, nil)
}
