package impl

import (
	"context"
	"github.com/kordar/gocrud"
	"github.com/redis/go-redis/v9"
	"github.com/spf13/cast"
)

type DictItemServiceCacheProxy struct {
	rdb redis.UniversalClient
	*DictItemServiceImpl
	redisKey string
}

func NewDictItemServiceCacheProxy(rdb redis.UniversalClient, dictItemServiceImpl *DictItemServiceImpl) *DictItemServiceCacheProxy {
	return &DictItemServiceCacheProxy{
		rdb:                 rdb,
		redisKey:            "gorm:dict_service_cache:options",
		DictItemServiceImpl: dictItemServiceImpl}
}

func (service DictItemServiceCacheProxy) Remove(body gocrud.RemoveBody) error {
	ctx := context.Background()
	object := cast.ToStringMapString(body.Data)
	service.rdb.HDel(ctx, service.redisKey, object["sign"])
	return service.DictItemServiceImpl.Remove(body)
}

func (service DictItemServiceCacheProxy) Create(body gocrud.FormBody) (interface{}, error) {
	ctx := context.Background()
	object := cast.ToStringMapString(body.Object)
	service.rdb.HDel(ctx, service.redisKey, object["sign"])
	return service.DictItemServiceImpl.Create(body)
}

func (service DictItemServiceCacheProxy) Update(body gocrud.FormBody) (interface{}, error) {
	ctx := context.Background()
	object := cast.ToStringMapString(body.Object)
	service.rdb.HDel(ctx, service.redisKey, object["sign"])
	return service.DictItemServiceImpl.Update(body)
}
