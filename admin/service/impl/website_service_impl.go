package impl

import (
	model "gitee.com/kordar/goadmin/admin/models"
	"gitee.com/kordar/goadmin/admin/vo"
	"github.com/kordar/gorbac"
	"gorm.io/gorm"
)

type WebsiteServiceImpl struct {
	db   *gorm.DB
	rbac gorbac.AuthManager
}

func NewWebsiteService(db *gorm.DB, rbac gorbac.AuthManager) *WebsiteServiceImpl {
	return &WebsiteServiceImpl{db, rbac}
}

func (service WebsiteServiceImpl) Menus(admin model.SysAdmin) vo.MenuVo {
	var data []vo.SettingVO
	tx := service.db.Model(&model.SysSetting{}).Where("active = 1 AND type = 1").Find(&data)
	menuVo := vo.MenuVo{
		Sidebar: make([]vo.SettingVO, 0),
		Areas:   make([]vo.SettingVO, 0),
		Welcome: make([]vo.SettingVO, 0),
	}
	if tx.Error != nil {
		return menuVo
	}

	for _, item := range data {

		if admin.Type != model.TypeSuper && item.URL != "#" && item.URL != "" {
			access := service.rbac.CheckAccess(nil, admin.ID, item.URL)
			if !access {
				continue
			}
		}

		if item.Position == int(model.SettingPositionSidebar) {
			menuVo.Sidebar = append(menuVo.Sidebar, item)
		}
		if item.Position == int(model.SettingPositionWelcome) {
			menuVo.Welcome = append(menuVo.Welcome, item)
		}
		if item.Position == int(model.SettingPositionArea) {
			menuVo.Areas = append(menuVo.Areas, item)
		}
	}
	return menuVo
}
