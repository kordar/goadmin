package impl

import (
	model "gitee.com/kordar/goadmin/admin/models"
	"gitee.com/kordar/goadmin/reader"
	"github.com/kordar/gocrud"
	gocrudgorm "github.com/kordar/gocrud-gorm"
	"gorm.io/gorm"
)

type SettingIconServiceImpl struct {
	db *gorm.DB
	gocrud.CommonResourceService
}

func NewSettingIconService(db *gorm.DB) *SettingIconServiceImpl {
	return &SettingIconServiceImpl{db: db}
}

func (service SettingIconServiceImpl) ResourceName() string {
	return "setting-icon"
}

func (service SettingIconServiceImpl) Search(body gocrud.SearchBody) gocrud.SearchVO {
	searchBody := gocrudgorm.NewGormSearchBody(body)
	db := searchBody.GormQuery(service.db, nil).Model(&model.SysSettingIcon{})
	queryReader := reader.NewQueryReader(db, body.Page, body.PageSize)
	var settingIcon []*model.SysSettingIcon
	return queryReader.SearchVO(settingIcon, nil)
	//queryReader.Reader(&settingIcon)
	//list := queryReader.Datalist(settingIcon, len(settingIcon), func(index int, item interface{}) map[string]interface{} {
	//	m := item.(*model.SysSettingIcon)
	//	return map[string]interface{}{
	//		"_serial": queryReader.AsSerial(index),
	//		"icon":    m.Name,
	//	}
	//})
	//return gocrud.NewSearchVO(list, queryReader.Count())
}

func (service SettingIconServiceImpl) Delete(body gocrud.RemoveBody) error {
	return body.Delete(&model.SysSettingIcon{}, service.db, nil)
}

//func (service SettingIconServiceImpl) Unique(ctx *gin.Context, data interface{}) error {
//	settingIcon := data.(*model.SysSettingIcon)
//	if service.db.Where("icon = ?", settingIcon.Name).First(settingIcon).Error == nil {
//		locale := ctx.DefaultQuery("locale", "en")
//		value := gocfg.GetGroupSectionValue(locale, "system.setting.validate", "icon.unique")
//		return errors.New(value)
//	}
//	return nil
//}

func (service SettingIconServiceImpl) Add(body gocrud.FormBody) (interface{}, error) {
	return body.CreateWithValid(&model.SysSettingIcon{}, service.db, nil, func(model interface{}) error {
		//return util.CtxGetValidParams(ctx, model, service, "Unique")
		return nil
	})
}

func (service SettingIconServiceImpl) Update(body gocrud.FormBody) (interface{}, error) {
	return body.Update(&model.SysSettingIcon{}, service.db, nil)
}

func (service SettingIconServiceImpl) Edit(body gocrud.EditorBody) error {
	return body.Updates(&model.SysSettingIcon{}, service.db, nil)
}
