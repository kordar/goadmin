package impl

import (
	model "gitee.com/kordar/goadmin/admin/models"
	"gitee.com/kordar/goadmin/reader"
	"github.com/kordar/gocrud"
	gocrudgorm "github.com/kordar/gocrud-gorm"
	"gorm.io/gorm"
)

type DictItemServiceImpl struct {
	db *gorm.DB
	gocrud.CommonResourceService
}

func NewDictItemService(db *gorm.DB) *DictItemServiceImpl {
	return &DictItemServiceImpl{db: db}
}

func (service DictItemServiceImpl) ResourceName() string {
	return "dict-item"
}

func (service DictItemServiceImpl) Search(body gocrud.SearchBody) gocrud.SearchVO {
	searchBody := gocrudgorm.NewGormSearchBody(body)
	tx := searchBody.GormQuery(service.db, nil).Model(&model.SysDictItem{})
	queryReader := reader.NewQueryReader(tx, body.Page, body.PageSize)
	var sysDict []*model.SysDictItem
	return queryReader.SearchVO(&sysDict, nil)
}

func (service DictItemServiceImpl) Remove(body gocrud.RemoveBody) error {
	return body.Delete(&model.SysDictItem{}, service.db, nil)
}

func (service DictItemServiceImpl) Create(body gocrud.FormBody) (interface{}, error) {
	return body.CreateWithValid(&model.SysDictItem{}, service.db, nil, func(model interface{}) error {
		//return util.CtxGetValidParams(ctx, model, service, "Unique")
		return nil
	})
}

func (service DictItemServiceImpl) Update(body gocrud.FormBody) (interface{}, error) {
	return body.Update(&model.SysDictItem{}, service.db.Select("Sign", "Label", "Value", "Data"), nil)
}
