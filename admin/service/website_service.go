package service

import (
	model "gitee.com/kordar/goadmin/admin/models"
	"gitee.com/kordar/goadmin/admin/vo"
)

type WebsiteService interface {
	Menus(admin model.SysAdmin) vo.MenuVo
}
