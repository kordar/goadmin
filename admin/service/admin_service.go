package service

import (
	admin "gitee.com/kordar/goadmin/admin/form"
	model "gitee.com/kordar/goadmin/admin/models"
	"mime/multipart"
)

type AdminService interface {
	Login(form admin.LoginForm) (model.SysAdmin, error)
	Register(form admin.RegisterForm) bool
	Avatar(id int64, file *multipart.FileHeader, admin model.SysAdmin) bool
	ResetPassword(form admin.ResetPasswordForm, admin model.SysAdmin) bool
}
