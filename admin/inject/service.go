package inject

import (
	"gitee.com/kordar/goadmin/admin/repository"
	"gitee.com/kordar/goadmin/admin/service/impl"
	digstarter "github.com/kordar/dig-starter"
	"github.com/kordar/gocrud"
	"github.com/kordar/gorbac"
)

func ServiceProvider() {

	digstarter.ProvideE(func(db GoAdminDB, rbac *gorbac.RbacService, adminRepos *repository.AdminRepository) (ServiceResult, error) {
		dictServiceImpl := impl.NewDictServiceCacheProxy(db.Redis, impl.NewDictService(db.DB))
		dictItemServiceImpl := impl.NewDictItemServiceCacheProxy(db.Redis, impl.NewDictItemService(db.DB))
		jumpwayServiceImpl := impl.NewJumpwayService(db.DB)
		languageServiceImpl := impl.NewLanguageService(db.DB)
		routerServiceImpl := impl.NewRouterService(db.DB, dictServiceImpl)
		settingIconServiceImpl := impl.NewSettingIconService(db.DB)
		settingServiceImpl := impl.NewSettingService(db.DB, dictServiceImpl)
		//uploadServiceImpl := impl.NewUploadService()
		websiteServiceImpl := impl.NewWebsiteService(db.DB, rbac.GetAuthManager())
		adminServiceImpl := impl.NewAdminService(db.DB, adminRepos, dictServiceImpl)
		return ServiceResult{
			JumpwayService:  jumpwayServiceImpl,
			LanguageService: languageServiceImpl,
			RouterService:   routerServiceImpl,
			SettingService:  settingServiceImpl,
			//UploadService:   uploadServiceImpl,
			WebsiteService:  websiteServiceImpl,
			DictService:     dictServiceImpl,
			DictItemService: dictItemServiceImpl,
			AdminService:    adminServiceImpl,
			ResourceService: []gocrud.ResourceService{
				jumpwayServiceImpl,
				languageServiceImpl,
				routerServiceImpl,
				settingIconServiceImpl,
				settingServiceImpl,
				dictServiceImpl,
				dictItemServiceImpl,
				adminServiceImpl,
			},
		}, nil
	})

}
