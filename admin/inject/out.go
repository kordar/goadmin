package inject

import (
	"gitee.com/kordar/goadmin/admin/controller"
	"gitee.com/kordar/goadmin/admin/repository"
	"gitee.com/kordar/goadmin/admin/service"
	"github.com/kordar/gocrud"
	"github.com/kordar/govalidator"
	"go.uber.org/dig"
)

type RepositoryResult struct {
	dig.Out
	AdminRepos *repository.AdminRepository
}

type ValidatorResult struct {
	dig.Out
	Validators []govalidator.IValidation `group:"govalidator,flatten"`
}

type ServiceResult struct {
	dig.Out
	service.JumpwayService
	service.LanguageService
	service.AdminService
	service.SettingService
	//service.UploadService
	service.RouterService
	service.SettingIconService
	service.WebsiteService
	service.DictService
	service.DictItemService
	ResourceService []gocrud.ResourceService `group:"resource-services,flatten"`
}

type ControllerResult struct {
	dig.Out
	*controller.AdminController
	*controller.RbacController
	*controller.AuthController
	*controller.WebsiteController
}
