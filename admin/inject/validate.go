package inject

import (
	digstarter "github.com/kordar/dig-starter"
	"github.com/kordar/govalidator"
)

func ValidatorProvider() {
	digstarter.ProvideE(func() (ValidatorResult, error) {
		return ValidatorResult{
			Validators: []govalidator.IValidation{
				&govalidator.NotEmptyValidation{},
				&govalidator.PasswordValidation{},
				&govalidator.PhoneValidation{},
			},
		}, nil
	})

}
