package inject

import (
	"gitee.com/kordar/goadmin/admin/controller"
	digstarter "github.com/kordar/dig-starter"
)

func ControllerProvider() {

	digstarter.ProvideE(func(p ControllerParams) (ControllerResult, error) {
		return ControllerResult{
			AdminController:   controller.NewAdminController(p.AdminService),
			RbacController:    controller.NewRbacController(p.Rbac),
			AuthController:    controller.NewAuthController(p.Captcha, p.Repos),
			WebsiteController: controller.NewWebsiteController(p.WebsiteService),
		}, nil
	})

}
