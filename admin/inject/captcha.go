package inject

import (
	digstarter "github.com/kordar/dig-starter"
	"github.com/mojocn/base64Captcha"
	"go.uber.org/dig"
)

func CaptchaProvider() {
	// 注入注册验证码
	digstarter.ProvideE(func() (*base64Captcha.Captcha, error) {
		var store = base64Captcha.DefaultMemStore
		var defaultDriverDigit = base64Captcha.NewDriverDigit(80, 240, 5, 0.7, 80)
		var captcha = base64Captcha.NewCaptcha(defaultDriverDigit, store)
		return captcha, nil
	}, dig.Name("captcha.login"))
}
