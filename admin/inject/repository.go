package inject

import (
	"gitee.com/kordar/goadmin/admin/repository"
	digstarter "github.com/kordar/dig-starter"
)

func RepositoryProvider() {
	digstarter.ProvideE(func(db GoAdminDB) (RepositoryResult, error) {
		return RepositoryResult{
			AdminRepos: repository.NewAdminRepository(db.DB),
		}, nil
	})
}
