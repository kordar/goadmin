package inject

import (
	digstarter "github.com/kordar/dig-starter"
	R "github.com/kordar/ginsys/resource"
)

func ResourceServiceInvoke() {
	digstarter.InvokeE(func(p ResourceServiceParams) {
		for _, s := range p.Services {
			R.Manager.AddResourceService(s)
		}
	})
}
