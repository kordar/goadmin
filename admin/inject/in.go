package inject

import (
	"gitee.com/kordar/goadmin/admin/controller"
	"gitee.com/kordar/goadmin/admin/repository"
	"gitee.com/kordar/goadmin/admin/service"
	"github.com/kordar/gocrud"
	"github.com/kordar/gorbac"
	"github.com/mojocn/base64Captcha"
	"github.com/redis/go-redis/v9"
	"go.uber.org/dig"
	"gorm.io/gorm"
)

type GoAdminDB struct {
	dig.In
	DB    *gorm.DB              `name:"mysql.sys"`
	Redis redis.UniversalClient `name:"goredis.sys"`
}

type ControllerParams struct {
	dig.In
	service.AdminService
	service.JumpwayService
	service.LanguageService
	service.RouterService
	service.SettingService
	service.SettingIconService
	//service.UploadService
	service.WebsiteService
	Captcha *base64Captcha.Captcha `name:"captcha.login"`
	Repos   *repository.AdminRepository
	Rbac    *gorbac.RbacService
}

type RouterParams struct {
	dig.In
	AuthCtrl    *controller.AuthController
	WebsiteCtrl *controller.WebsiteController
	AdminCtrl   *controller.AdminController
	RbacCtrl    *controller.RbacController
	AdminRepos  *repository.AdminRepository
	Rbac        *gorbac.RbacService
}

type ResourceServiceParams struct {
	dig.In
	Services []gocrud.ResourceService `group:"resource-services"`
}
