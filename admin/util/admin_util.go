package util

import (
	"fmt"
	"github.com/kordar/goutil"
	"math/rand"
	"time"
)

func GenerateSalt() int {
	rand.Seed(time.Now().UTC().UnixNano())
	return 100000 + rand.Intn(899999)
}

func GeneratePassword(password string, salt string) string {
	return goutil.Md5V(password + salt)
}

func GenerateAvatar() string {
	rand.Seed(time.Now().UTC().UnixNano())
	number := 1 + rand.Intn(12)
	return fmt.Sprintf("/avatars/%02d.png", number)
}
