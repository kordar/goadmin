package util

import (
	"errors"
	"github.com/kordar/gocfg"
	"io/ioutil"
	"mime/multipart"
)

// DomainImage 相对路径转域名
func DomainImage(path string) string {
	domain := gocfg.GetSectionValue("upload", "domain")
	return domain + path
}

func ReadByteFromFileHeader(file *multipart.FileHeader) ([]byte, error) {
	src, err := file.Open()
	defer src.Close()

	if err != nil {
		return nil, err
	}

	fd, err := ioutil.ReadAll(src)
	if err == nil {
		return fd, err
	}

	return nil, errors.New("read error")
}
