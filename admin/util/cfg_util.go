package util

import (
	"strings"
)

func GetPrefixCfg(section map[string]interface{}, prefix string) map[string]interface{} {
	config := map[string]interface{}{}
	for k, v := range section {
		if strings.HasPrefix(k, prefix) {
			key := strings.Replace(k, prefix, "", 1)
			config[key] = v
		}
	}
	return config
}
