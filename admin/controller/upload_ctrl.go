package controller

import (
	admin "gitee.com/kordar/goadmin/admin/form"
	"github.com/gin-gonic/gin"
	"github.com/kordar/ginsys/util"
	response "github.com/kordar/goframework_resp"
)

// PullList 拉取列表
func PullList(ctx *gin.Context) {
	f := admin.PullListForm{}
	if err := util.DefaultGetValidParams(ctx, &f); err != nil {
		response.Error(ctx, err.Error(), nil)
		return
	}

	//uploadService := impl.NewUploadService()
	//list := uploadService.List(f.BucketName, f.Path, f.Next)
	//response.Success(ctx, "", list)
}

// DeleteObject 删除对象
func DeleteObject(ctx *gin.Context) {
	f := admin.DeleteObjectForm{}
	if err := util.DefaultGetValidParams(ctx, &f); err != nil {
		response.Error(ctx, err.Error(), nil)
		return
	}

	//uploadService := impl.NewUploadService()
	//if err := uploadService.Remove(f.BucketName, f.Name); err == nil {
	//	response.Success(ctx, "success", nil)
	//} else {
	//	response.Error(ctx, err.Error(), nil)
	//}
}
