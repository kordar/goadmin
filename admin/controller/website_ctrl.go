package controller

import (
	"gitee.com/kordar/goadmin/admin/enums"
	model "gitee.com/kordar/goadmin/admin/models"
	"gitee.com/kordar/goadmin/admin/service"
	"gitee.com/kordar/goadmin/admin/util"
	"github.com/gin-gonic/gin"
	response "github.com/kordar/goframework_resp"
)

type WebsiteController struct {
	service service.WebsiteService
}

func NewWebsiteController(service service.WebsiteService) *WebsiteController {
	return &WebsiteController{service: service}
}

func (c WebsiteController) Menus(ctx *gin.Context) {
	admin := ctx.MustGet(enums.AdminKey).(model.SysAdmin)
	menus := c.service.Menus(admin)
	response.Success(ctx, "", menus)
}

func (c WebsiteController) UserInfo(ctx *gin.Context) {
	admin := ctx.MustGet(enums.AdminKey).(model.SysAdmin)
	response.Success(ctx, "", map[string]interface{}{
		"id":       admin.ID,
		"phone":    admin.Phone,
		"username": admin.Username,
		"avatar":   util.DomainImage(admin.Avatar),
		"type":     admin.Type,
	})
}
