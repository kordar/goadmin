package admin

import (
	"github.com/kordar/gormext"
)

type LoginForm struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
	Code     string `form:"code" json:"code" binding:"required"`
	Token    string `form:"token" json:"token" binding:"required"`
}

type RegisterForm struct {
	Username string `form:"username" json:"username" binding:"required"`
	Phone    string `form:"phone" json:"phone" binding:"required,phone"`
	Password string `form:"password" json:"password" binding:"required"`
}

type ResetPasswordForm struct {
	Id       gormext.StrInt64 `form:"id" json:"id" binding:"required"`
	Password string           `form:"password" json:"password" binding:"required,password"`
}

type AvatarForm struct {
	Id  gormext.StrInt64 `form:"id" json:"id" binding:"required"`
	Img string           `form:"img" json:"img" binding:"required"`
}
