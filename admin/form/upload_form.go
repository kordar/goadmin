package admin

type PullListForm struct {
	BucketName string `json:"bucket_name" validate:"required"`
	Path       string `form:"path" json:"path"`
	Next       string `form:"next" json:"next"`
}

type DeleteObjectForm struct {
	BucketName string `json:"bucket_name" validate:"required"`
	Name       string `json:"name" validate:"required"`
}
