package admin

type PhoneForm struct {
	Phone string `form:"phone" json:"phone" validate:"required,phone" chn:"phone"`
	Lang  string `form:"locale" json:"locale" validate:"required" chn:"lang"`
}
