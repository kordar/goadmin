package admin

import (
	model "gitee.com/kordar/goadmin/admin/models"
	"github.com/kordar/gormext"
)

type SettingForm struct {
	ID         gormext.StrInt `json:"id"`
	Title      string         `json:"title" binding:"required"`
	Icon       string         `json:"icon"`
	Type       gormext.StrInt `json:"type"`
	Position   gormext.StrInt `json:"position"`
	Pid        gormext.StrInt `json:"pid"`
	RouterName string         `json:"router_name"`
	URL        string         `json:"url"`
	Active     gormext.StrInt `json:"active"`
	Sort       gormext.StrInt `json:"sort"`
}

func (s SettingForm) Target() interface{} {
	return &model.SysSetting{
		ID:         int(s.ID),
		Title:      s.Title,
		Icon:       s.Icon,
		Type:       int(s.Type),
		Position:   int(s.Position),
		Pid:        int(s.Pid),
		RouterName: s.RouterName,
		URL:        s.URL,
		Active:     int(s.Active),
		Sort:       int(s.Sort),
	}
}

type SysRouterForm struct {
	Name string `json:"name" binding:"required"`
	URL  string `json:"url" binding:"required"`
	Type string `json:"type" binding:"required"`
	Data string `json:"data"`
}

func (s SysRouterForm) Target() interface{} {
	return &model.SysRouter{
		Name: s.Name,
		URL:  s.URL,
		Type: s.Type,
		Data: s.Data,
	}
}
