package admin

import "github.com/kordar/gormext"

type FastAddPermissionForm struct {
	Resource string `json:"resource" form:"resource" binding:"required"`
	Name     string `json:"name" form:"name" binding:"required"`
}

type BatchItemsForm struct {
	Names []string `form:"names" json:"names" binding:"notempty"`
}

type RoleForm struct {
	Name        string `form:"name" json:"name" binding:"required"`
	Description string `form:"description" json:"description"`
	RuleName    string `form:"rule_name" json:"rule_name"`
}

type UpdateRoleForm struct {
	SourceName  string `form:"source_name" json:"source_name" binding:"required"`
	TargetName  string `form:"target_name" json:"target_name" binding:"required"`
	Description string `form:"description" json:"description"`
	RuleName    string `form:"rule_name" json:"rule_name"`
}

type PermissionForm struct {
	Name        string `form:"name" json:"name" binding:"required"`
	Description string `form:"description" json:"description"`
	RuleName    string `form:"rule_name" json:"rule_name"`
}

type UpdatePermissionForm struct {
	SourceName  string `form:"source_name" json:"source_name" binding:"required"`
	TargetName  string `form:"target_name" json:"target_name" binding:"required"`
	Description string `form:"description" json:"description"`
	RuleName    string `form:"rule_name" json:"rule_name"`
}

type RuleForm struct {
	Name        string `json:"name" binding:"required"`
	ExecuteName string `json:"execute_name"`
}

type UpdateRuleForm struct {
	SourceName  string `json:"source_name" binding:"required"`
	TargetName  string `json:"target_name" binding:"required"`
	ExecuteName string `json:"execute_name" binding:"required"`
}

type AssignItemForm struct {
	Roles       []string         `json:"roles"`
	Permissions []string         `json:"permissions"`
	Id          gormext.StrInt64 `json:"id" binding:"required"`
}

type AddPermissionChildrenForm struct {
	Children []string `json:"children" binding:"notempty"`
	Name     string   `json:"name" binding:"required"`
}
