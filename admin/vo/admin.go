package vo

import (
	"time"
)

type SysAdminVO struct {
	ID         int64     `json:"id"`
	Phone      string    `json:"phone"`
	Avatar     string    `json:"avatar"`
	Username   string    `json:"username"`
	Password   string    `json:"password"`
	Status     int       `json:"status"`
	Type       int       `json:"type"`
	Version    int       `json:"version"`
	Deleted    int       `json:"deleted"`
	CreateTime time.Time `json:"create_time" formatter:"time"`
	UpdateTime time.Time `json:"update_time" formatter:"time"`
}
