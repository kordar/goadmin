package vo

import "time"

type SettingVO struct {
	ID         int       `json:"id"`
	Title      string    `json:"title"`
	Icon       string    `json:"icon"`
	Position   int       `json:"position"`
	Type       int       `json:"type"`
	Pid        int       `json:"pid"`
	RouterName string    `json:"router_name"`
	URL        string    `json:"url"`
	Active     int       `json:"active"`
	Sort       int       `json:"sort"`
	CreateTime time.Time `json:"create_time" formatter:"time"`
	UpdateTime time.Time `json:"update_time" formatter:"time"`
}

type SysRouterVO struct {
	Name       string    `json:"name"`
	URL        string    `json:"url"`
	Type       string    `json:"type"`
	Data       string    `json:"data"`
	CreateTime time.Time `json:"create_time" formatter:"time"`
	UpdateTime time.Time `json:"update_time" formatter:"time"`
}
