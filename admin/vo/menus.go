package vo

type MenuVo struct {
	Sidebar []SettingVO `json:"sidebar"`
	Areas   []SettingVO `json:"areas"`
	Welcome []SettingVO `json:"welcome"`
}
