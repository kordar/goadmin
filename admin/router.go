package admin

import (
	"gitee.com/kordar/goadmin/admin/controller"
	"gitee.com/kordar/goadmin/admin/inject"
	"gitee.com/kordar/goadmin/admin/middleware"
	"github.com/gin-gonic/gin"
	digstarter "github.com/kordar/dig-starter"
	ginsys_starter "github.com/kordar/ginsys-starter"
	middleware2 "github.com/kordar/ginsys/middleware"
	"github.com/kordar/ginsys/resource"
	"go.uber.org/dig"
)

func routerProvider() {
	digstarter.ProvideE(func() (func(router *gin.Engine), error) {
		return Router, nil
	}, dig.Group(ginsys_starter.RouterIocGroup.Value()))
}

func Router(router *gin.Engine) {
	group := router.Group("/")
	digstarter.InvokeE(func(p inject.RouterParams) {
		router.POST("/login/captcha", p.AuthCtrl.Captcha)
		router.GET("/login/captcha", p.AuthCtrl.Captcha)
		router.POST("/login/submit", p.AuthCtrl.Login)
		router.POST("/login/config", p.AuthCtrl.Config)
		group.Use(middleware2.RecoveryMiddleware(), middleware.AdminAuth(p.AdminRepos, p.Rbac.GetAuthManager()))
		{
			group.POST("/resource/:apiName/info", resource.GetInfo)
			group.POST("/resource/:apiName/list", resource.GetList)
			group.POST("/resource/:apiName/add", resource.Add)
			group.POST("/resource/:apiName/update", resource.Update)
			group.POST("/resource/:apiName/delete", resource.Delete)
			group.POST("/resource/:apiName/edit", resource.Edit)
			group.POST("/resource/:apiName/configs", resource.Configs)

			group.POST("/menus", p.WebsiteCtrl.Menus)
			group.POST("/admin/user-info", p.WebsiteCtrl.UserInfo)

			group.POST("/admin/register", p.AdminCtrl.Register)
			group.POST("/admin/reset-password", p.AdminCtrl.ResetPassword)
			group.POST("/admin/upload-avatar", p.AdminCtrl.UploadAvatar)

			// -------------------------------- RBAC ---------------------------
			group.POST("/rbac/assign", p.RbacCtrl.AssignItemToUser)
			// -------------------------------- role ----------------------------
			group.POST("/rbac/roles", p.RbacCtrl.Roles)
			group.POST("/rbac/add-role", p.RbacCtrl.AddRole)
			group.GET("/rbac/get-roles-by-user", p.RbacCtrl.GetRolesByUser)
			group.POST("/rbac/update-role", p.RbacCtrl.UpdateRole)
			group.POST("/rbac/delete-role", p.RbacCtrl.DeleteRole)
			group.POST("/rbac/batch-delete-role", p.RbacCtrl.BatchDeleteRoles)
			// --- permission ---
			group.POST("/rbac/permissions", p.RbacCtrl.Permissions)
			group.GET("/rbac/get-children", p.RbacCtrl.GetChildren)
			group.POST("/rbac/add-permission-children", p.RbacCtrl.AddPermissionChildren)
			group.GET("/rbac/get-permissions-by-user", p.RbacCtrl.GetPermissionsByUser)
			group.POST("/rbac/add-permission", p.RbacCtrl.AddPermission)
			group.POST("/rbac/update-permission", p.RbacCtrl.UpdatePermission)
			group.POST("/rbac/delete-permission", p.RbacCtrl.DeletePermission)
			group.POST("/rbac/batch-delete-permission", p.RbacCtrl.BatchDeletePermissions)
			group.POST("/rbac/fast-add-permission", p.RbacCtrl.FastAddPermission)
			// ---- rule ----
			group.POST("/rbac/rules", p.RbacCtrl.Rules)
			group.POST("/rbac/add-rule", p.RbacCtrl.AddRule)
			group.POST("/rbac/update-rule", p.RbacCtrl.UpdateRule)
			group.POST("/rbac/delete-rule", p.RbacCtrl.DeleteRule)
			// ----- upload ----
			group.POST("/upload/pull-list", controller.PullList)
			group.POST("/upload/delete-object", controller.DeleteObject)
		}
	})
}
