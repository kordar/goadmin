package model

import (
	"database/sql/driver"
	"encoding/json"
	"time"
)

type AdminType int8

const (
	TypeOrdinary AdminType = 0
	TypeSuper    AdminType = 9
)

func (t AdminType) String() string {
	switch t {
	case TypeOrdinary:
		return "admin.ordinary"
	case TypeSuper:
		return "admin.super"
	default:
		return "admin.unknown"
	}
}

// SysAdmin [...]
type SysAdmin struct {
	ID         int64     `gorm:"autoIncrement:true;primaryKey;column:id;type:bigint(20);not null" json:"id"`
	Phone      string    `gorm:"unique;column:phone;type:varchar(32);not null" json:"phone"`
	Avatar     string    `gorm:"column:avatar;type:varchar(255);default:null" json:"avatar"`
	Username   string    `gorm:"unique;column:username;type:varchar(64);not null" json:"username"`
	Password   string    `gorm:"column:password;type:varchar(255);default:null" json:"password"`
	Status     int       `gorm:"column:status;type:int(11);default:null;default:0" json:"status"`
	Type       AdminType `gorm:"column:type;type:int(11);default:null;default:0" json:"type"`
	Salt       string    `gorm:"column:salt;type:varchar(32);default:null" json:"salt"`
	Version    int       `gorm:"column:version;type:int(11);default:null;default:0" json:"version"`
	Deleted    int       `gorm:"column:deleted;type:int(11);default:null;default:0" json:"deleted"`
	CreateTime time.Time `gorm:"column:create_time;type:datetime;default:null;autoCreateTime" json:"createTime"`
	UpdateTime time.Time `gorm:"column:update_time;type:datetime;default:null;autoUpdateTime" json:"updateTime"`
}

// TableName get sql table name.获取数据库表名
func (m *SysAdmin) TableName() string {
	return "sys_admin"
}

// SysAdminToken [...]
type SysAdminToken struct {
	Token  string `gorm:"primaryKey;column:token;type:varchar(32);not null"`
	UserID int64  `gorm:"column:user_id;type:bigint(20);default:null"`
}

// TableName get sql table name.获取数据库表名
func (m *SysAdminToken) TableName() string {
	return "sys_admin_token"
}

// SysAdminTenant [...]
type SysAdminTenant struct {
	UID    int64  `gorm:"primaryKey;column:uid;type:bigint(20);not null"`
	Tenant string `gorm:"primaryKey;index:tenant;column:tenant;type:varchar(32);not null"`
}

// TableName get sql table name.获取数据库表名
func (m *SysAdminTenant) TableName() string {
	return "sys_admin_tenant"
}

// -----------------------------------------------

type MapJson struct {
	V map[string]string // 如果有需要，将Url和Name的需求扩展到v里，比如v map[string]string
}

func (m *MapJson) Value() (driver.Value, error) {
	b, err := json.Marshal(m.V)
	return string(b), err
}

func (m *MapJson) Scan(input interface{}) error {
	d := map[string]string{}
	err := json.Unmarshal([]byte(input.(string)), &d)
	m.V = d
	return err
}

type JumpwayType string

var (
	JumpwayNone  JumpwayType = "none"
	JumpwayToken JumpwayType = "token"
	JumpwayPage  JumpwayType = "page"
)

// SysJumpway [...]
type SysJumpway struct {
	ID     uint32  `gorm:"autoIncrement:true;primaryKey;column:id;type:int(10) unsigned;not null"`
	Name   string  `gorm:"column:name;type:varchar(128);not null;default:''"`
	Type   string  `gorm:"column:type;type:varchar(24);not null;default:{}"`
	Page   string  `gorm:"column:page;type:varchar(255);not null;default:''"`
	Params MapJson `gorm:"column:params;type:text;default:null"`
}

// TableName get sql table name.获取数据库表名
func (m *SysJumpway) TableName() string {
	return "sys_jumpway"
}

const TableNameSysRouter = "sys_router"

// SysRouter mapped from table <sys_router>
type SysRouter struct {
	Name       string    `gorm:"column:name;primaryKey" json:"name"`
	URL        string    `gorm:"column:url" json:"url"`
	Type       string    `gorm:"column:type;primaryKey" json:"type"`
	Data       string    `gorm:"column:data;type:text" json:"data"`
	CreateTime time.Time `gorm:"column:create_time;autoCreateTime" json:"create_time"`
	UpdateTime time.Time `gorm:"column:update_time;autoUpdateTime" json:"update_time"`
}

// TableName SysRouter's table name
func (*SysRouter) TableName() string {
	return TableNameSysRouter
}

const TableNameSysSetting = "sys_setting"

// SysSetting mapped from table <sys_setting>
type SysSetting struct {
	ID         int       `gorm:"column:id;primaryKey;autoIncrement:true"`
	Title      string    `gorm:"column:title;not null"`
	Icon       string    `gorm:"column:icon"`
	Position   int       `gorm:"column:position"`
	Type       int       `gorm:"column:type"`
	Pid        int       `gorm:"column:pid"`
	RouterName string    `gorm:"column:router_name"`
	URL        string    `gorm:"column:url"`
	Active     int       `gorm:"column:active"`
	Sort       int       `gorm:"column:sort"`
	CreateTime time.Time `gorm:"column:create_time;autoCreateTime"`
	UpdateTime time.Time `gorm:"column:update_time;autoUpdateTime"`
}

// TableName SysSetting's table name
func (*SysSetting) TableName() string {
	return TableNameSysSetting
}

// SysSettingIcon [...]
type SysSettingIcon struct {
	Name string `gorm:"primaryKey;column:name;type:varchar(128);not null"`
}

// TableName get sql table name.获取数据库表名
func (m *SysSettingIcon) TableName() string {
	return "sys_setting_icon"
}

const TableNameSysLanguage = "sys_language"

// SysLanguage mapped from table <sys_language>
type SysLanguage struct {
	ID       int64  `gorm:"column:id;primaryKey" json:"id"`
	Key      string `gorm:"column:key;not null;comment:所属类型" json:"key"`         // 所属类型
	Language string `gorm:"column:language;not null;comment:语言" json:"language"` // 语言
	Value    string `gorm:"column:value;not null;comment:值" json:"value"`        // 值
}

// TableName SysLanguage's table name
func (*SysLanguage) TableName() string {
	return TableNameSysLanguage
}

const TableNameSysDict = "sys_dict"

// SysDict 字典
type SysDict struct {
	ID         int64     `gorm:"column:id;primaryKey;autoIncrement:true" json:"id"`
	Name       string    `gorm:"column:name;not null;comment:字典名称" json:"name"`                                               // 字典名称
	Sign       string    `gorm:"column:sign;not null;comment:字典唯一编号" json:"sign"`                                             // 字典唯一编号
	Remark     string    `gorm:"column:remark;comment:字典备注" json:"remark"`                                                    // 字典备注
	CreateTime time.Time `gorm:"column:create_time;not null;comment:创建时间;autoCreateTime" json:"create_time" formatter:"time"` // 创建时间
	UpdateTime time.Time `gorm:"column:update_time;not null;comment:更新时间;autoUpdateTime" json:"update_time" formatter:"time"` // 更新时间
}

// TableName SysDict's table name
func (*SysDict) TableName() string {
	return TableNameSysDict
}

const TableNameSysDictItem = "sys_dict_item"

// SysDictItem 字典项
type SysDictItem struct {
	ID    int64  `gorm:"column:id;primaryKey;autoIncrement:true" json:"id"`
	Sign  string `gorm:"column:sign;not null;comment:关联字典标识" json:"sign"` // 关联字典标识
	Label string `gorm:"column:label;not null;comment:字典项键" json:"label"` // 字典项键
	Value string `gorm:"column:value;not null;comment:字典项值" json:"value"` // 字典项值
	Data  string `gorm:"column:data;not null;comment:字典项值扩展" json:"data"`
}

// TableName SysDictItem's table name
func (*SysDictItem) TableName() string {
	return TableNameSysDictItem
}
