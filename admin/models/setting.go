package model

type SettingPosition int

var (
	SettingPositionSidebar SettingPosition = 1
	SettingPositionArea    SettingPosition = 2
	SettingPositionWelcome SettingPosition = 3
)
