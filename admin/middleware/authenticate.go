package middleware

import (
	"gitee.com/kordar/goadmin/admin/enums"
	model "gitee.com/kordar/goadmin/admin/models"
	"gitee.com/kordar/goadmin/admin/repository"
	"github.com/gin-gonic/gin"
	response "github.com/kordar/goframework_resp"
	jwt "github.com/kordar/gojwt"
	"github.com/kordar/gorbac"
	"github.com/spf13/cast"
)

func AdminAuth(repos *repository.AdminRepository, rbac gorbac.AuthManager) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var ok bool
		if ok, _ = checkAccess(ctx, repos, rbac); !ok {
			response.Unauthorized(ctx, "", nil)
			ctx.Abort()
			return
		}

		// --------------------------

		ctx.Next()
		return
	}
}

func checkAccess(ctx *gin.Context, repos *repository.AdminRepository, rbac gorbac.AuthManager) (bool, model.SysAdmin) {

	tokenValue := ctx.GetHeader("Token")
	claims, err := jwt.ParseToken(tokenValue, enums.HmacSampleSecret)
	if err == nil {
		id := cast.ToInt64(claims["id"])
		if admin, err3 := repos.FindOne(id); err3 == nil {
			ctx.Set(enums.AdminKey, admin)
			if admin.Type == model.TypeSuper {
				return true, admin
			}

			access := rbac.CheckAccess(ctx, id, ctx.Request.URL.Path)
			if access {
				return true, admin
			}
		}
	}

	return false, model.SysAdmin{}
}
