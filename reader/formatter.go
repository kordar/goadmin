package reader

import (
	"strconv"
	"strings"
	"time"
)

var (
	AllOfGender    = 0 // 所有性别
	MaleOfGender   = 1 // 男性性别
	FemaleOfGender = 2 // 女性性别
)

type Formatter struct {
}

func (f *Formatter) AsDate(timestamp int64) string {
	return time.Unix(timestamp, 0).Format("2006-01-02")
}

func (f *Formatter) AsDatetime(timestamp int64) string {
	return time.Unix(timestamp, 0).Format("2006-01-02 15:04:05")
}

func (f *Formatter) AsCheckListString(list string, labels map[int]string) string {
	args := strings.Split(list, ",")
	var s = make([]string, len(args))
	for i, v := range args {
		k, _ := strconv.Atoi(v)
		s[i] = labels[k]
	}
	return strings.Join(s, ", ")
}

func (f *Formatter) AsSexLabel(sex int) string {
	switch sex {
	case MaleOfGender:
		return "男"
	case FemaleOfGender:
		return "女"
	default:
		return "保密"
	}
}
