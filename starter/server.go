package starter

import (
	"fmt"
	"gitee.com/kordar/goadmin/admin"
	command_starter "github.com/kordar/command-starter"
	digstarter "github.com/kordar/dig-starter"
	ginsys_starter "github.com/kordar/ginsys-starter"
	"github.com/kordar/gocfg"
	gocfgmodule "github.com/kordar/gocfg-load-module"
	goframework_goredis "github.com/kordar/goframework-goredis"
	goframeworkgormmysql "github.com/kordar/goframework-gorm-mysql"
	"github.com/kordar/gorbac"
	gorbac_starter "github.com/kordar/gorbac-starter"
	goredis_starter "github.com/kordar/goredis-starter"
	mysql_starter "github.com/kordar/mysql-starter"
	"github.com/redis/go-redis/v9"
	"go.uber.org/dig"
	"gorm.io/gorm"
)

func ServerWithStarter(starters []gocfgmodule.GoCfgModule, requirements []string, dependency []string) {
	requirements = append(requirements, "ioc")
	dependency = append(dependency, "ioc")
	ginsys_starter.ServerStarter(starters, requirements, dependency)
}

func Server(requirements []string, dependency []string) {
	gormLogLevel := gocfg.GetSystemValue("gorm_log_level")
	requirements = append(requirements, "ioc")
	dependency = append(dependency, "ioc")

	ginsys_starter.ServerStarter([]gocfgmodule.GoCfgModule{

		// 注册dig组件
		digstarter.NewDigModule("ioc", nil),

		// 注册rbac组件
		gorbac_starter.NewRbacModule("gorbac", func(moduleName string, item map[string]interface{}) {
			digstarter.ProvideE(func() (*gorbac.RbacService, error) {
				return gorbac_starter.GetRbacService(), nil
			})
		}, "mysql", "goredis"),

		// 注册mysql组件
		mysql_starter.NewMysqlModule("mysql", func(moduleName string, itemId string, item map[string]string) {
			name := fmt.Sprintf("%s.%s", moduleName, itemId)
			digstarter.ProvideE(func() (*gorm.DB, error) {
				return goframeworkgormmysql.GetMysqlDB(itemId), nil
			}, dig.Name(name))
		}, gormLogLevel),

		// 命令行启动
		command_starter.CommandModule{},

		// 注册redis组件
		goredis_starter.NewRedisModule("goredis", func(moduleName string, itemId string, item map[string]string) {
			name := fmt.Sprintf("%s.%s", moduleName, itemId)
			digstarter.ProvideE(func() (redis.UniversalClient, error) {
				return goframework_goredis.GetRedisClient(itemId), nil
			}, dig.Name(name))
		}),

		// goadmin组件初始化
		admin.GoAdminModule{},

		// 上传组件
		//uploadstarter.UploadModule{},
		// 异步任务
		//async_task_starter.AsyncTaskStarter{},

		// redis注册中心
		//registry_starter.RegistryModule{},
	}, requirements, dependency)

}
