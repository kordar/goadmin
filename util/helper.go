package util

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
)

func GetMapStr(m map[string]interface{}, field string, value string) string {
	if m[field] == "" {
		return value
	} else {
		return cast.ToString(m[field])
	}
}

func GetMapInt(m map[string]interface{}, field string, value int) int {
	if m[field] == "" {
		return value
	} else {
		return cast.ToInt(m[field])
	}
}

func GetTenantId(ctx *gin.Context) int64 {
	if value, exists := ctx.Get("tenantId"); exists {
		return cast.ToInt64(value)
	}
	return 0
}
